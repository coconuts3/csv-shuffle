import csv
from logging import Logger
from typing import List, Tuple, Protocol, Iterable, Dict
import polars as pl
from polars.exceptions import ColumnNotFoundError

class TableManipulator(Protocol):
    source_file: Iterable
    shuffled_file: Iterable

    def read(self, filename: str) -> None:
        pass

    def validate_shuffle_run(self, transformations: List[Tuple]) -> None:
        pass

    def shuffle(self, transformations: List[Tuple]) -> None:
        pass

    def write_to_file(self, filename: str) -> None:
        pass

class CsvTableManipulator(TableManipulator):
    source_file: pl.DataFrame
    shuffled_file: Dict

    def __init__(self):
        self.logger = Logger('csv-shuffle')
        self.shuffled_file = {}
        self.source_file = []

    def read(self, filename: str) -> None:
        with open(filename) as f:
            reader = csv.DictReader(f)
            self.source_file = pl.DataFrame(list(reader))

    def validate_shuffle_run(self, transformations: List[Tuple]) -> None:
        if len(self.source_file) == 0:
            raise AttributeError("Source file has not been specified.")

        faulty_source_columns = []
        for trans in transformations:
            source, _ = trans
            if source not in self.source_file:
                faulty_source_columns.append(source)

        if len(faulty_source_columns) > 0:
            raise ValueError(f'Provided source columns are faulty: {" | ".join(faulty_source_columns)}')

    def shuffle(self, transformations: List[Tuple]) -> None:
        shuffled_file = {}
        for trans in transformations:
            source, target = trans
            try:
                shuffled_file[target] = self.source_file.get_column(source)
            except ColumnNotFoundError as e:
                self.logger.warning(str(e))

        self.shuffled_file = shuffled_file

    def write_to_file(self, filename: str) -> None:
        with open(filename, 'w') as f:
            w = csv.writer(f)
            w.writerow(self.shuffled_file.keys())
            w.writerows(zip(*self.shuffled_file.values()))


if __name__ == '__main__' :
    filename = 'example.csv'
    trans = [("Product ID", "ID"), ("Product ID", "P-ID"), ("Retail Price", "Price"), ("Search Keywords", "kwords"), ("Date Created", "date")]
    faulty_trans = [("test", "test")]

    man = CsvTableManipulator()
    man.read(filename)
    man.validate_shuffle_run(trans)
    man.shuffle(trans)
    man.write_to_file("shuffled_example.csv")


