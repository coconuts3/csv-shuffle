import std/parsecsv
import std/tables
import std/strutils

type
  CsvTableManipulator* = object
   source_file: OrderedTable[string, seq[string]]
   shuffled_file: OrderedTable[string, seq[string]]

proc toString(sources: seq[string], seperator: string): string =
  var sourcesAsString: string = ""
  for source in sources:
    sourcesAsString = sourcesAsString & source & seperator

  return sourcesAsString.strip(chars= {' ', '\t', '\v', '\r', '\l', '\f', '|', ','})

proc read*(self: var CsvTableManipulator, filename: string) =
  var
    p: CsvParser

  p.open(filename)
  p.readHeaderRow()
  while p.readRow():
    for col in items(p.headers):
      try:
        self.source_file[col].add(p.rowEntry(col))
      except  KeyError:
        self.source_file[col] = @[p.rowEntry(col)]
  p.close()


proc validateShuffleRun*(self: var CsvTableManipulator, transformations: seq[(string, string)]) =
  if len(self.source_file) == 0:
    raise newException(ValueError, "Source file has not been read!")

  var
    faulty_sources: seq[string] = @[]

  for trans in transformations:
    var (source, target) = trans
    if source in self.source_file:
      continue
    faulty_sources.add(source)

  if len(faulty_sources) > 0:
    let msg: string = toString(faulty_sources, " | ")
    raise newException(ValueError, "Provided source headers are faulty: " & msg)

proc shuffle*(self: var CsvTableManipulator, transformations: seq[(string, string)]) =
  for trans in transformations:
    var (source, target) = trans
    try:
      self.shuffled_file[target] = self.source_file[source]
    except  KeyError:
      echo "Provided column is not present in the source file: ", source


proc writeToFile*(self: var CsvTableManipulator, filename: string) =
  var
    w: int
    fileUlt: seq[seq[string]]
    i: int = 0
    fileStr: string = ""
    keysArr: seq[string]

  for v in self.shuffled_file.values:
    w = v.len
    break

  for k in self.shuffled_file.keys:
    keysArr.add(k)

  fileStr = fileStr & toString(keysArr, ",") & "\n"
  while true:
    if i >= w:
      break
    var f: seq[string]
    for v in self.shuffled_file.values:
      f.add("\"" & v[i] & "\"")
    fileUlt.add(f)

    i = i + 1

  for row in fileUlt:
    fileStr = fileStr & toString(row, ",") & "\n"

  writeFile(filename, fileStr)



when is_main_module:
  var
    csvMan: CsvTableManipulator
    faulty_trans: seq[(string, string)]
    trans: seq[(string, string)]

  faulty_trans = @[("test", "test")]
  trans = @[("Product ID", "ID"), ("Product ID", "P-ID"), ("Retail Price", "Price"), ("Search Keywords", "kwords"), ("Date Created", "date")]
  csvMan = CsvTableManipulator(source_file : initOrderedTable[string, seq[string]](), shuffled_file: initOrderedTable[string, seq[string]]())

  csvMan.read("example.csv")
  csvMan.validateShuffleRun(trans)
  csvMan.shuffle(trans)
  csvMan.writeToFile("shuffled_example.csv")